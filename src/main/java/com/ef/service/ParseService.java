package com.ef.service;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ef.batch.BatchConfiguration;
import com.ef.util.Constants;

@Service
public class ParseService {
	
	@Autowired
	private BatchConfiguration batchConfiguration;

	@Autowired
	private JobLauncher jobLauncher;

	public BatchStatus startJob() {
//		REAL PARAMETERS TO BE PUT HERE
		try {
			JobParameters jobParameters = new JobParametersBuilder()
					.addString(Constants.START_DATE_PARAMETER, "daily")
					.addString(Constants.START_DATE_PARAMETER, "2017-01-01.15:00:00")
					.addDouble(Constants.THRESHOLD_PARAMETER, new Double(1))
					.addString(Constants.FILE_PATH_PARAMETER, "/media/martinkrol/Martin/accesss.log").toJobParameters();
			JobExecution execution = jobLauncher.run(batchConfiguration.parseJob(), jobParameters);
			return execution.getStatus();
		} catch (Exception e) {
			e.printStackTrace();
			return  BatchStatus.FAILED;
		}
	}

}
