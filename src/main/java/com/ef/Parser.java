package com.ef;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.batch.BatchAutoConfiguration;

import com.ef.service.ParseService;

@SpringBootApplication(exclude = {BatchAutoConfiguration.class})
public class Parser implements CommandLineRunner {
	
	@Autowired
	ParseService parseService;

	public static void main(String[] args) throws Exception {
		SpringApplication app = new SpringApplication(Parser.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
	}
	
	public void run(String... args) throws Exception {
		System.out.println("Exit status : " + parseService.startJob());
    }
}
