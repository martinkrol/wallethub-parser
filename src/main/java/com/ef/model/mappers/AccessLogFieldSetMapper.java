package com.ef.model.mappers;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import com.ef.model.entities.AccessLog;
import com.ef.util.Constants;

public class AccessLogFieldSetMapper implements FieldSetMapper<AccessLog> {
    public AccessLog mapFieldSet(FieldSet fieldSet) {
    	AccessLog accessLog = new AccessLog();
    	accessLog.setIp(fieldSet.readString(1));
    	try {
			accessLog.setDate(new SimpleDateFormat(Constants.DEFAULT_DATE_FORMAT).parse(fieldSet.readString(0)));
		} catch (ParseException e) {
			System.out.println("Failed to retrieve Date");
		}
        return accessLog;
    }
}

