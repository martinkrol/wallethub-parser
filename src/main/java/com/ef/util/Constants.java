package com.ef.util;

public class Constants {

//	INPUT PARAMETERS
	public static final String FILE_PATH_PARAMETER = "accesslog";
	public static final String START_DATE_PARAMETER = "startDate";
	public static final String DURATION_PARAMETER = "duration";
	public static final String THRESHOLD_PARAMETER = "threshold";
	
	public static final String NO_FILE_STATUS = "noFilePath";
	public static final String FILE_FOUND_STATUS = "fileFound";

	public static final String TOO_MANY_REQUESTS_MESSAGE = "Too many requests";

	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	
	

}
