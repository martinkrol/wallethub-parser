package com.ef.batch.writer;

import javax.persistence.EntityManagerFactory;

import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ef.model.entities.BlockedIp;

@Component
public class ParseLogDataWriter {

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	public ItemWriter<BlockedIp> getParseLogWriter() {
		JpaItemWriter<BlockedIp> writer = new JpaItemWriter<BlockedIp>();
		writer.setEntityManagerFactory(entityManagerFactory);
		return writer;
	}

}
