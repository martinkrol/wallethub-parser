package com.ef.batch.writer;

import javax.persistence.EntityManagerFactory;

import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ef.model.entities.AccessLog;

@Component
public class ParseFileDataWriter {

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	public ItemWriter<AccessLog> getParseFileWriter() {
		JpaItemWriter<AccessLog> writer = new JpaItemWriter<AccessLog>();
		writer.setEntityManagerFactory(entityManagerFactory);
		return writer;
	}

}
