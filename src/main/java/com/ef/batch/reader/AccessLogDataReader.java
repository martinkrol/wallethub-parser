package com.ef.batch.reader;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.stereotype.Component;

import com.ef.provider.CustomJpaQueryProvider;
import com.ef.util.Constants;

@Component("AccessLogDataReader")
@StepScope
public class AccessLogDataReader extends JpaPagingItemReader<String>{
	
	public AccessLogDataReader(EntityManagerFactory entityManagerFactory) throws Exception {
		this.setEntityManagerFactory(entityManagerFactory);
	}
	
    @BeforeStep
    public void test(StepExecution stepExecution) throws Exception{
    	Map<String, Object> queryParams = new HashMap<>();
    	queryParams.put(Constants.THRESHOLD_PARAMETER, stepExecution.getJobParameters().getString(com.ef.util.Constants.THRESHOLD_PARAMETER));
    	CustomJpaQueryProvider<String> jpaQueryProvider = new CustomJpaQueryProvider<String>();
		jpaQueryProvider.setQuery("AccessLog.findAll");
		this.setParameterValues(queryParams);
		this.setQueryProvider(jpaQueryProvider);
		this.afterPropertiesSet();
    }

}
