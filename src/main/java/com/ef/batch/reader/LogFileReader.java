package com.ef.batch.reader;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

import com.ef.model.entities.AccessLog;
import com.ef.model.mappers.AccessLogFieldSetMapper;


@Component("LogFileReader")
@StepScope
public class LogFileReader extends FlatFileItemReader<AccessLog>{
	
	@Value("#{jobParameters['accesslog']}")
	String logPath;
	
	public LogFileReader(){
	    DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer();
	    delimitedLineTokenizer.setDelimiter("|");
		DefaultLineMapper<AccessLog> lineMapper = new DefaultLineMapper<AccessLog>();
		lineMapper.setLineTokenizer(delimitedLineTokenizer);
		lineMapper.setFieldSetMapper(new AccessLogFieldSetMapper());
		this.setLineMapper(lineMapper);
		
	}
	
    @BeforeStep
    public void test(StepExecution stepExecution){
    	System.out.println("weee");
    	this.setResource(new FileSystemResource(logPath));
    	this.open(new ExecutionContext());
    }

	@Override
	public AccessLog read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		return super.read();
	}
	

}
