package com.ef.batch;

import java.io.File;

import javax.persistence.EntityManager;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import com.ef.batch.processor.AccessLogProcessor;
import com.ef.batch.reader.AccessLogDataReader;
import com.ef.batch.writer.ParseFileDataWriter;
import com.ef.batch.writer.ParseLogDataWriter;
import com.ef.model.entities.AccessLog;
import com.ef.model.entities.BlockedIp;
import com.ef.util.Constants;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration extends DefaultBatchConfigurer {

	@Autowired
	@Qualifier("LogFileReader")
	FlatFileItemReader<AccessLog> logFileReader;
	
	@StepScope
	@Autowired
	@Qualifier("AccessLogDataReader")
	public JpaPagingItemReader<String> accessLogDataReader() throws Exception {
		return new AccessLogDataReader(entityManager.getEntityManagerFactory());
	}

	@Autowired
	private ParseLogDataWriter parseLogDataProvider;

	@Autowired
	private ParseFileDataWriter parseFileDataProvider;

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private EntityManager entityManager;

	public PlatformTransactionManager jpaTransactionManager() {
		return new JpaTransactionManager(entityManager.getEntityManagerFactory());
	}

	@Override
	protected JobRepository createJobRepository() throws Exception {
		MapJobRepositoryFactoryBean factory = new MapJobRepositoryFactoryBean();
		factory.afterPropertiesSet();
		return factory.getObject();
	}

	Step parseFileStep() throws Exception {
		System.out.println("Parsing file");
		return stepBuilderFactory.get("parseFileStep").transactionManager(jpaTransactionManager())
				.<AccessLog, AccessLog>chunk(1000).reader(logFileReader)
				.writer(parseFileDataProvider.getParseFileWriter()).build();
	}

	Step parseLogStep() throws Exception {
		System.out.println("Parsing log");
		return stepBuilderFactory.get("parseLogStep").transactionManager(jpaTransactionManager())
				.<String, BlockedIp>chunk(1).reader(accessLogDataReader())
				.processor(new AccessLogProcessor()).writer(parseLogDataProvider.getParseLogWriter()).build();
	}

	Step checkParamsStep() {
		System.out.println("Checking params");
		return stepBuilderFactory.get("checkParamsStep").tasklet((contribution, chunkContext) -> {
			JobParameters jobParameters = chunkContext.getStepContext().getStepExecution().getJobParameters();
			if (jobParameters.getString(Constants.FILE_PATH_PARAMETER) != null){
				if (new File(jobParameters.getString(Constants.FILE_PATH_PARAMETER)).exists()){
					contribution.setExitStatus(new ExitStatus(Constants.FILE_FOUND_STATUS));
				} else{
					contribution.setExitStatus(new ExitStatus(Constants.NO_FILE_STATUS));
				}
			} else {
				contribution.setExitStatus(new ExitStatus(Constants.NO_FILE_STATUS));
			}
			return RepeatStatus.FINISHED;
		}).build();
	}

	public Job parseJob() throws Exception {
		return jobBuilderFactory.get("parseJob")
				.flow(checkParamsStep()).on(Constants.FILE_FOUND_STATUS).to(parseFileStep())
				.from(parseFileStep()).next(parseLogStep())
				.from(checkParamsStep()).on(Constants.NO_FILE_STATUS).to(parseLogStep())
				.from(parseLogStep())
				.end().build();
	}
}