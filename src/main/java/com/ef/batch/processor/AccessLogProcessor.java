package com.ef.batch.processor;

import org.springframework.batch.item.ItemProcessor;

import com.ef.model.entities.BlockedIp;
import com.ef.util.Constants;

public class AccessLogProcessor implements ItemProcessor<String, BlockedIp> {

	@Override
	public BlockedIp process(final String accessLog) throws Exception {
		BlockedIp blockedIp = new BlockedIp();
		blockedIp.setIp(accessLog);
		System.out.println(accessLog);
		blockedIp.setReason(Constants.TOO_MANY_REQUESTS_MESSAGE);
		return blockedIp;
	}
	
	

}
